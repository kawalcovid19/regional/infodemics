# 2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository of [Provinsi Jawa Barat](https://jabarprov.go.id/) by [KawalCovid19.id](http://kawalcovid19.id)
This is the data repository for the 2019 Novel Coronavirus which hosts Covid-19 reports Pemerintah Daerah Jawa Barat collected and analyzed by [KawalCovid19.id](http://kawalcovid19.id)

## Data Sources:
Pemerintah Daerah Jawa Barat: https://pikobar.jabarprov.go.id/

## Categories:
There are four categories of data available

1. Raw data in Time Series. Originally taken from the source and presented in time-series (daily). See `covid19_raw_time_series`
2. Raw data latest. Original taken from the source with latest in non-historical. See `covid19_raw_latest`
3. Uniform in Time Series. Processed from the raw in uniform format and presented in time-series. See `covid19_unform_time_series`
4. Uniform latest. Processed from the raw in uniform format which depict the latest snapshot. See `covid19_uniform_latest`

## Followed Us:

*  [Instagram](https://www.instagram.com/kawalcovid19.id/)

*  [Twitter](https://twitter.com/KawalCOVID19)

*  [Facebook](https://www.facebook.com/KawalCOVID19)

## Terms of Use:
This Gitlab repo and its contents herein, including all data, mapping, and analysis are owned by respective Local Government of Indonesia, all rights reserved, is provided to the public strictly for educational and academic research purposes. The Website relies upon publicly available data from multiple sources, that do not always agree. [KawalCOVID19.id](http://kawalcovid19.id) hereby disclaims any and all representations and warranties with respect to the Website, including accuracy, fitness for use, and merchantability. Reliance on the Website for medical guidance or use of the Website in commerce is strictly prohibited.

## Copyright
[KawalCOVID19.id](http://kawalcovid19.id) disebarluaskan di bawah [Lisensi Creative Commons Atribusi-NonKomersial-TanpaTurunan 4.0 Internasional](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.id)