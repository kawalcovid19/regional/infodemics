# 2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository of [Provinsi Jawa Barat](https://jabarprov.go.id/) by [KawalCovid19.id](http://kawalcovid19.id)
This is the data repository for the 2019 Novel Coronavirus which hosts Covid-19 reports Pemerintah Daerah Jawa Barat collected and analyzed by [KawalCovid19.id](http://kawalcovid19.id)

Uniform latest data is standardized from the raw which depict the latest snapshot of the infodemics. The objective is to drive information into a database and record it there in a consistent, predictable, and homogenous way. This common format enables collaborative research, large-scale analytics, and sharing of sophisticated tools and methodologies. With more consistent data, it becomes easier, faster, and more cost-efficient to analyze data, find trends, target, nurture, and convert.


## Last Update
03 May 2020 22:00 GMT +1

## District-Level (Kabupaten) Data Latest
Filename: latest_covid19_uniform_jabar_district_gender.csv

Columns name is standardized and added calculated columns for easy plotting. All records are the latest snapshot from specified district

| Columns           | Description    |
| ---------------- |----------------|
| province | Province name |
| district | District name (Kabupaten / Kota) |
| status | Status which consist of ODP, PDP, OTG or Positif |	
| stage | Stage of each status |	
| male | Total cases for male |
| female | Total cases for female |

## District-Level (Kabupaten) Data Latest
Filename: latest_covid19_uniform_jabar_district_groupage.csv

Columns name is standardized and added calculated columns for easy plotting. All records are the latest snapshot from specified district

| Columns           | Description    |
| ---------------- |----------------|
| province | Province name |
| district | District name (Kabupaten / Kota) |
| status | Status which consist of ODP, PDP, OTG or Positif |	
| stage | Stage of each status |	
| GrupNone | Total cases for which doesn't have data for group age |
| Grup<10 | Total cases for group age < 10 years old |
| Grup10-20 | Total cases for group age 10 < x <= 20 years old |
| Grup20-30 | Total cases for group age 20 < x <= 30 years old |
| Grup30-40 | Total cases for group age 30 < x <= 40 years old |
| Grup40-50 | Total cases for group age 40 < x <= 50 years old |
| Grup50-60 | Total cases for group age 50 < x <= 60 years old |
| Grup60-70 | Total cases for group age 60 < x <= 70 years old |
| Grup70-80 | Total cases for group age 70 < x <= 80 years old |
| Grup80-90 | Total cases for group age 80 < x <= 90 years old |
| Grup90-100 | Total cases for group age 90 < x <= 100 years old |
| Grup>100 | Total cases for group age > 100 years old |


## Data Sources:
Pemerintah Daerah Jawa Barat: https://pikobar.jabarprov.go.id/

## Followed Us:

*  [Instagram](https://www.instagram.com/kawalcovid19.id/)

*  [Twitter](https://twitter.com/KawalCOVID19)

*  [Facebook](https://www.facebook.com/KawalCOVID19)

## Terms of Use:
This Gitlab repo and its contents herein, including all data, mapping, and analysis are owned by respective Local Government of Indonesia, all rights reserved, is provided to the public strictly for educational and academic research purposes. The Website relies upon publicly available data from multiple sources, that do not always agree. [KawalCOVID19.id](http://kawalcovid19.id) hereby disclaims any and all representations and warranties with respect to the Website, including accuracy, fitness for use, and merchantability. Reliance on the Website for medical guidance or use of the Website in commerce is strictly prohibited.

## Copyright
[KawalCOVID19.id](http://kawalcovid19.id) disebarluaskan di bawah [Lisensi Creative Commons Atribusi-NonKomersial-TanpaTurunan 4.0 Internasional](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.id)